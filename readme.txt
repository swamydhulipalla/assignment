This folder named "assignment_solutions" consist of following deliverables:

1. Jupyther notebook with the completed assignment : "assignment_solutions.ipynb"
2. List of packages required for runtime (txt file): "requirements.txt"
3. Code used for exposing REST api for prediction: "lightgbm_predict.py"



