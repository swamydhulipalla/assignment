# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 11:15:53 2021

@author: Swamy
"""
#How to run:
#set FLASK_APP=C:\Users\sd186063\Desktop\Application\lightgbm.py
#flask run
import os
import joblib
from flask import Flask, jsonify, request
from flask_restful import  Api, Resource, reqparse

cwd = os.getcwd()
app = Flask(__name__)

#loading model 
model = joblib.load(f"{cwd}\lgbr_cars.model")

#parsing arguments
parser = reqparse.RequestParser()
parser.add_argument('query', type=int,action='append')

@app.route('/predict', methods = ['POST'])
def predict():
    '''
    used to predict used car price from the given test data
    '''
    
    test_data = []
    args = parser.parse_args()
    test_data.append(args['query'])
    print('testdata:',test_data)
    predicted_price = model.predict(test_data)
        
    return str(round(predicted_price[0],2))

if __name__ == '__main__':
    app.run(port=5000,debug=True)   
