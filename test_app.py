import requests as rq
import unittest
import pandas.util.testing as tm

#model_test_input1 = [[3,1,190,-1,125000,5,3,1]]
model_test_input2 = [[-1,1,0,118,150000,0,1,38]]

#url = "http://127.0.0.1:5000/predict"
url = "https://car-price-predict21.herokuapp.com:5000/predict"

#params1 = {'query': model_test_input1}
params2 = {'query': model_test_input2}


response = rq.post(url,params2)

print(response.text)

'''

response = rq.post(url,params1)
predicted1 = response.json()['Prediction']
print(predicted1)

response = rq.post(url,params2)
predicted2 = response.json()['Prediction']
print(predicted2)
'''

#tc = unittest.TestCase('__init__')


#tc.assertAlmostEqual(predicted1, 14026.35, places=2)
#tc.assertAlmostEqual(predicted2, 13920.70, places=2)

#print("All tests are passed")
